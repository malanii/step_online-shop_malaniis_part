
let cardCount = document.getElementById('cardCountItem').textContent;

let modalDiv = document.createElement('div');

modalDiv.className = "modal-wrapper";
document.body.appendChild(modalDiv);

if (+cardCount === 0){

modalDiv.innerHTML = ' <div class="modal fade modal " id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"\n' +
    '         aria-hidden="true">\n' +
    '        <div class="modal-dialog " role="document">\n' +
    '            <div class="modal-content modal-wrapper-empty">\n' +
    '\n' +
    '                <div class="modal-body modal-body-class-empty">\n' +
    '                    <p class="modal-window-cross text-center align-middle" data-dismiss="modal" aria-hidden="true"\n' +
    '                       aria-label="Close">x</p>\n' +
    '                    <p class="modal-window-header text-center mb-0">Your cart is empty</p>\n' +
    '                    <p class="modal-window-text text-center mb-0">But it\'s never too late to fix it :)\n' +
    '                    </p>\n' +
    '                </div>\n' +
    '\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>';

 }
if (+cardCount > 0) {
    modalDiv.innerHTML = '<div class="modal fade modal " id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"\n' +
        '     aria-hidden="true">\n' +
        '    <div class="modal-dialog " role="document">\n' +
        '\n' +
        '        <div class="modal-content modal-wrapper-full">\n' +
        '\n' +
        '            <div class=" col-12 modal-body modal-body-class bg-white ">\n' +
        '                <p class="modal-window-cross text-center align-middle" data-dismiss="modal" aria-hidden="true"\n' +
        '                   aria-label="Close">x</p>\n' +
        '\n' +
        '                <div class="modal-window-full-card-product ">\n' +
        '                    <div class="container a">\n' +
        '\n' +
        '                        <div class=" col-lg-6 d-flex flex-wrap justify-content-between">\n' +
        '                            <img class="col-6 col-sm-4 col-lg-5 p-1" src="img/modal_example/card_example.png"\n' +
        '                                 height="114" width="162"/>\n' +
        '\n' +
        '                            <div class="col-6 col-lg-7 p-0 align-items-center">\n' +
        '                                <p class="modal-window-full-product-title font-weight-bold">Sunbaby Magic Chair</p>\n' +
        '                                <div class="modal-window-full-product-price-block">\n' +
        '                                    <p class="modal-window-full-product-price modal-window-full-product-price-through text-left ">\n' +
        '                                        $350.00</p>\n' +
        '                                    <p id="actualPrice" class="modal-window-full-product-price modal-window-full-product-price-actually text-center font-weight-bold">\n' +
        '                                        $250.00</p>\n' +
        '                                </div>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                        <div class=" col-lg-6 d-flex align-items-center modal-total-quantity-and-sum pt-3">\n' +
        '                            <div class="modal-window-full-product-quantity-block">\n' +
        '                                <p class="modal-window-full-product-quantity align-middle mr-1 mt-2 mb-0">Quantity:</p>\n' +
        '                                <div class="modal-window-full-card-count-block d-flex">\n' +
        '                                    <p id="quantity" class="modal-window-full-card-count text-center mr-2 border border-dark  mb-0">\n' +
        '                                        2</p>\n' +
        '                                    <p  id="minusItem" class="modal-window-full-card-count-minus text-center mr-2 font-weight-bold border mb-0 border-dark">\n' +
        '                                         -</p>\n' +
        '                                    <p  id="plusItem"class="modal-window-full-card-count-plus text-center font-weight-bold mb-0">+</p>\n' +
        '                                </div>\n' +
        '                            </div>\n' +
        '                            <div>\n' +
        '                                <p class="modal-window-full-product-price modal-window-full-card-sum text-center  p-0">\n' +
        '                                    Sum</p>\n' +
        '                                <p class="modal-window-full-product-price modal-window-full-card-general-sum font-weight-bold text-center p-0">\n' +
        '                                    $250</p>\n' +
        '\n' +
        '                            </div>\n' +
        '\n' +
        '                        </div>\n' +
        '                    </div>\n' +
        '                </div>\n' +
        '                <p class=" text-right my-3 ">Total:$1000</p>\n' +
        '                <div class="d-flex justify-content-between">\n' +
        '                    <button type="button" class="btn btn-secondary text-uppercase btn-modal-continue"\n' +
        '                            data-dismiss="modal"><i class="fa fal fa-angle-right mr-2"></i>continue\n' +
        '                    </button>\n' +
        '                    <button type="button" class="btn btn-info btn-modal-buy text-uppercase text-white"><i\n' +
        '                            class="fa far fa-shopping-basket mr-2"></i>Buy now\n' +
        '                    </button>\n' +
        '                </div>\n' +
        '\n' +
        '            </div>\n' +
        '\n' +
        '        </div>\n' +
        '    </div>\n' +
        '</div>'
}

let quantity = document.getElementById('quantity').innerHTML;
let price = document.getElementById('actualPrice').textContent;
let minus = document.getElementById('minusItem').textContent;
let plus = document.getElementById('plusItem').textContent;


console.log(quantity);
console.log(quantity.length);
console.log(typeof(quantity));



